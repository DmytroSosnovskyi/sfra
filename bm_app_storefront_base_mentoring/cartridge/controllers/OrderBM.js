const ISML = require('dw/template/ISML');
const ordersSearchHelper = require('*/cartridge/scripts/helpers/ordersSearchHelper');
const forms = session.getForms();
const OrdersBmModel = require('*/cartridge/models/OrdersBmModel');
const URLUtils = require('dw/web/URLUtils');
const Resource = require('dw/web/Resource');
var hm = request.httpParameterMap;

// End point, render tamplate orders and pass some parametes to the pdict.
function show () {
    var queryObject = ordersSearchHelper.getSearchQuery(hm);
    var pageSize = hm.pagesize.intValue || 10
    var currentPage = hm.page.intValue || 1;
    pageSize = pageSize == 0 ? 5000 : pageSize;
    var orderBmModel = new OrdersBmModel(queryObject);

    var pagingModel =  new dw.web.PagingModel(orderBmModel.orders, orderBmModel.orders.getCount());
    pagingModel.setPageSize(pageSize);
    pagingModel.setStart(pageSize * (currentPage - 1));

    var searchOrderForm = forms['searchOrder'];
    ISML.renderTemplate('orders.isml', {
        errorMessage: orderBmModel.orders.count === 0 ? Resource.msg('order.error.empty', 'order', null) : null,
        pagingModel: pagingModel,
        searchOrderForm: searchOrderForm,
        showAction: URLUtils.url('OrderBM-Show').toString(),
        getDetailsAction: URLUtils.url('OrderBM-GetDetails').toString()
    })
}

// End point, returns json object with order ot orders.
function getDetails () {
    const printWriten = response.getWriter();
    if (hm.isParameterSubmitted('orderNo')) {
        var queryObject = {
            orderNo: hm['orderNo'].value
        }
        var isDetailed = true;

        var orderBmModel = new OrdersBmModel(queryObject, isDetailed);

        printWriten.println(JSON.stringify(orderBmModel));
    } else {
        printWriten.println(JSON.stringify({
            errorMessage: Resource.msg('order.error.notsubmitted', 'order', null)
        }));
    }

}

show.public = true;
getDetails.public = true;

module.exports = {
    GetDetails: getDetails,
    Show: show
}
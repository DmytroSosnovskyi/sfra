function getSearchQuery (hm) {
    var query;
    if (!hm['orderNo'].empty || !hm['dwfrm_searchOrder_orderNo'].empty) {
        query = {
            orderNo: hm['orderNo'].value || hm['dwfrm_searchOrder_orderNo'].value
        }
    } else if (!hm['email'].empty) {
        query = {
            email:  hm['email'].value
        }
    } else if (!hm['dwfrm_searchOrder_startDate'].empty && !hm['dwfrm_searchOrder_endDate'].empty) {
        query = {
            startDate: hm['dwfrm_searchOrder_startDate'].value,
            endDate: hm['dwfrm_searchOrder_endDate'].value
        }
    } else if (!hm['name'].empty) {
        query = {
            name: hm['name'].value
        }
    } else if (!hm['status'].empty) {
        query = {
            status: hm['status'].value
        }
    } else {
        query = {};
    }
    return query;
}

module.exports = {
    getSearchQuery: getSearchQuery
}
const js_form_input = document.querySelector('.js-form__input');
const js_from_select = document.querySelector('.js-form__select');
const js_order_search = document.querySelector('.js-order-search');
const js_label = document.querySelector('.js-label');
const js_input = document.querySelector('.js-input');
const js_select = document.querySelector('.js-select');
const js_input_date = document.querySelector('.js-input-date');
const js_order_link = document.querySelectorAll('.js-order-link');
const showErrorNotificationWindow = require('./errorNotificationWindow');
const displayPopup = require('./popup');
let popup = document.createElement('div');
let isPopup = false;

/**
 * Gets links with order number, and fetch the request to get information for popup menu.
 */

const handleLinks = () => {
    for (let i = 0; i < js_order_link.length; i++) {
        js_order_link[i].onclick  = e => {
            if (isPopup) {
                deletePopup(popup);
            }
            e.preventDefault();
            fetch(js_order_link[i].getAttribute('href') + '&' + 'orderNo=' + js_order_link[i].innerHTML)
            .then(res => res.json())
            .then(data => {
                if (data.errorMessage) {
                    showErrorNotificationWindow(data.errorMessage, 'red');
                }
                displayPopup(data.orders, popup, createTableRow, deletePopup);
                isPopup = true;
            })
        }
    }
}

/**
 * Control witch search item should be active and get it to the input label.
 */

const controlSearchItem = () => {
    js_order_search.onclick = e => {
        let link = e.target.closest('a');
        if (link.getAttribute('value') === 'status') {
            js_from_select.setAttribute('name', link.getAttribute('value'));
        } else {
            js_form_input.setAttribute('name', link.getAttribute('value'));
        }
        js_label.innerHTML = link.innerHTML;

        if (link.innerHTML === 'Status') {
            if (js_select.classList.contains('hidden')) {
                js_select.classList.remove('hidden');
                js_input.classList.remove('visible');
                js_input_date.classList.remove('visible')
                js_input_date.classList.add('hidden')
            }
            js_select.classList.add('visible');
            js_input.classList.add('hidden');
        } else if (link.innerHTML === 'Date') {
            if (js_input_date.classList.contains('hidden')) {
                js_input_date.classList.remove('hidden');
                js_input.classList.remove('visible');
                js_select.classList.remove('visible');
                js_select.classList.add('hidden');
            }
            js_input_date.classList.add('visible');
            js_input.classList.add('hidden');
        } else {
            if (js_input.classList.contains('hidden')) {
                js_input.classList.remove('hidden');
            }
            js_select.classList.add('hidden');
            js_input.classList.add('visible');
            js_input_date.classList.add('hidden');
        }

        for (let i = 0; i < e.target.closest('div').childNodes.length; i++) {
            if (e.target.closest('div').childNodes[i].className === 'order__active-link' || link.className !== 'order__active-link') {
                e.target.closest('div').childNodes[i].className = 'order__switch-link';
            }
        }
        if (link.className === 'order__switch-link') {
            link.classList.remove('order__switch-link');
            link.classList.add('order__active-link');
        }
    }
}

/**
 * Runs functions.
 */
const init = () => {
    handleLinks();
    controlSearchItem();
}

init();

/**
 * Creates rows of table.
 * @param {*} columnsValue
 * @param {*} isHeader
 * @param {*} table
 * @param {*} createLink
 */

const createTableRow = (columnsValue, isHeader, table, createLink) => {
    let table_row_th = document.createElement('tr');
    if (isHeader) {
        table_row_th.classList.add('order__table-row-th');
    } else {
        table_row_th.classList.add('order__table-row');
    }
    for (let i = 0; i < columnsValue.length; i++) {
    let table_row;
    let isLink = false;
    if (isHeader) {
        table_row = document.createElement('th');
        table_row.classList.add('order__table-header');
    } else {
        table_row = document.createElement('td');
        table_row.classList.add('order__table-data');
    }
    if (i === 0 && !isHeader && createLink) {
        table_row.innerHTML = '<a class=' + 'link' + ' href=' + js_form.getAttribute('action') +
        '?orderNo=' + columnsValue[i] + '&isDetailed=' + true +  '>' + columnsValue[i] + '</a>';
        if (!isLink) {
            let link = table_row.closest('td').querySelector('a');
            isLink = true;
            handleLink(link);
        }
    } else {
        table_row.innerHTML = columnsValue[i];
    }
    table_row_th.append(table_row);
}
table.append(table_row_th);
}

/**
 * Deletes the popup menu from dom.
 * @param {*} popup The Popup will deleted.
 */

const deletePopup = (popup) => {
    while(popup.firstChild) {
        popup.firstChild.remove()
    }
    popup.remove();
    isPopup = false;
}





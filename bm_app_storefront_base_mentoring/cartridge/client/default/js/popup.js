/**
 * Creates popup window for order detail.
 * @param {*} order Object with order detail.
 * @param {*} popup Dom element.
 * @param {function} createTableRow Creates talble rows.
 * @param {function} deletePopup Deletes popup from dom.
 */

const displayPopup = (order, popup, createTableRow, deletePopup) => {
    popup.classList.add('popup');
    let popupOrderDetail = document.createElement('div');
    popupOrderDetail.classList.add('popup__orderDetail');
    let popupOrderDetails = document.createElement('div');
    popupOrderDetails.classList.add('popup__orderDetails')
    let orderDetailsheader = document.createElement('span');
    orderDetailsheader.classList.add('popup__header');
    orderDetailsheader.innerHTML = 'Order details';
    popupOrderDetail.append(orderDetailsheader);
    var popup_close_btn = document.createElement('button');
    popup_close_btn.classList.add(('popup__btn'));
    popup_close_btn.innerHTML = 'X';
    popup.append(popup_close_btn);

    for (let param in order[0]) {
        if (typeof order[0][param] !== 'object') {
            if (popupOrderDetails.childNodes.length >= 7) {
                popupOrderDetail.append(popupOrderDetails);
                popupOrderDetails = document.createElement('div');
                popupOrderDetails.classList.add('popup__orderDetails');
            }

            let orderDetailsParam = document.createElement('span');
            orderDetailsParam.classList.add('popup__param');
            orderDetailsParam.innerHTML = `${param}-${order[0][param]}`;
            popupOrderDetails.append(orderDetailsParam);
        }
    }
    popupOrderDetail.append(popupOrderDetails);

    popup.append(popupOrderDetail);

    let popupProductDetail = document.createElement('div');
    popupProductDetail.classList.add('popup__orderDetail');
    let productTable = document.createElement('table');
    productTable.classList.add('popup__product-table');
    let productHeaders = Object.keys(order[0].products[0])
    createTableRow(productHeaders, true, productTable);
    order[0].products.forEach(product => {
        let productBodyHeaders = [];
        for (let header in product) {
            productBodyHeaders.push(product[header]);
        }
        createTableRow(productBodyHeaders, false,  productTable, false);
    })
    popupProductDetail.append(productTable);
    popup.append(popupProductDetail);

    document.body.append(popup);

    popup_close_btn.onclick = e => {
        deletePopup(popup);
    };
}

module.exports = displayPopup;
/**
 * Created popup and adds it it the DOM.
 * @param {*} text
 * @param {*} color
 */

const popup = (text, color) => {
    console.log(text);
    let dom_el = document.createElement('div');
    dom_el.classList.add('error-notification');
    let el_text = document.createElement('span');
    el_text.classList.add('error-notification__text');
    el_text.style.background = color;
    el_text.innerHTML = text;
    dom_el.append(el_text);
    document.body.append(dom_el);

    setInterval(() => {
        dom_el.remove();
    },5000)
}

module.exports = popup;
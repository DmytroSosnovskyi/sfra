const Site = require('dw/system/Site').getCurrent();
const Orders_orderConstant = Site.getCustomPreferenceValue('Orders_orderConstant') || 'ORDER_STATUS_NEW';
const Orders_queryStringStatus = Site.getCustomPreferenceValue('Orders_queryString') || 'status={0}';
const Orders_queryStringEmail = Site.getCustomPreferenceValue('Orders_queryString_email') || 'customerEmail ILIKE {0}';
const Orders_queryStringName = Site.getCustomPreferenceValue('Orders_queryString_name') || 'customerEmail ILIKE {0}';
const Orders_queryStringOrderNo = Site.getCustomPreferenceValue('Orders_queryString_orderNo') || 'orderNo ILIKE {0}';
const Orders_queryStringDate = Site.getCustomPreferenceValue('Orders_queryString_date') || 'creationDate >= {0} AND creationDate <= {1}';
const OrderMgr = require('dw/order/OrderMgr');
const Order = require('dw/order/Order');
const SystemObjectMgr  = require ('dw/object/SystemObjectMgr')

/**
 *  Gets product details for specific order.
 * @param {*} productLineItems
 */
function getProductDetails (productLineItems) {
    var products = [];
    productLineItems.toArray().forEach(function(productLineItem) {
        products.push({
            pdi: productLineItem.productID,
            productName: productLineItem.productName,
            taxRate: productLineItem.taxRate,
            taxBasis: productLineItem.taxBasis.value,
            price: productLineItem.price.value
        })
    })
    return products;
}

/**
 * Creates array with products detail.
 * @param {*} productLineItems An product line items.
 * @return {Array} Array of products detail.
 */

function getOrderDetails (orders) {
    var data = [];
    while(orders.hasNext()) {
        let order = orders.next();
        data.push({
            OrderNo: order.orderNo,
            CreationDate: order.getCreationDate().toDateString(),
            SiteContext: Site.ID,
            CustomerName: order.getCustomerName(),
            OrderStatus: order.status.displayValue,
            CustomerEmail: order.getCustomerEmail(),
            Total: order.getTotalGrossPrice().getValue(),
            products: getProductDetails(order.productLineItems),
            CreatedBy: order.createdBy,
            IpAddress: order.remoteHost,
            CustomerNo: order.customerNo ? order.customerNo : '',
            CustomerPhone: order.customer.profile ? order.customer.profile.phoneHome : '',
            ShippingStatus: order.shippingStatus.displayValue,
            ExportStatus: order.exportStatus.displayValue,
            ConfirmationStatus: order.confirmationStatus.displayValue
        })
    }
    return data;
}

/**
 * Created array with orders or object with errors.
 * @param {*} queryObject Value of query parameter.
 * @returns {Object}
 */
function searchOrders (queryObject, isDetailed) {
    var orders;
    if (Object.keys(queryObject).length === 0 && queryObject[Object.keys(queryObject)[0]] !== '') {
        orders = OrderMgr.searchOrders(Orders_queryStringStatus,'creationDate desc', Order[Orders_orderConstant]);
    } else if (Object.keys(queryObject)[0] === 'status') {
        orders = orders = OrderMgr.searchOrders(Orders_queryStringStatus,'creationDate desc', Order[queryObject.status]);
    } else if (Object.keys(queryObject)[0] === 'email') {
        orders = orders = OrderMgr.searchOrders(Orders_queryStringEmail,'creationDate desc', queryObject.email);
    } else if (Object.keys(queryObject)[0] === 'name') {
        orders = orders = OrderMgr.searchOrders(Orders_queryStringName,'creationDate desc', queryObject.name);
    } else if (Object.keys(queryObject)[0] === 'orderNo') {
        orders = orders = OrderMgr.searchOrders(Orders_queryStringOrderNo,'creationDate desc', queryObject.orderNo);
    } else {
        orders = SystemObjectMgr.querySystemObjects('Order', Orders_queryStringDate, 'creationDate desc', queryObject.startDate, queryObject.endDate);
    }

    if (isDetailed) {
        return getOrderDetails(orders);
    } else {
        return orders;
    }
}

/**
 * Constructor. Creates objects with order or orders;
 * @param {*} queryObject
 * @param {*} isDetailed
 */

function OrdersBmModel (queryObject, isDetailed) {
    this.orders = searchOrders(queryObject, isDetailed);
}

module.exports = OrdersBmModel;
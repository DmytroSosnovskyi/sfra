'use strict'

const URLUtils = require('dw/web/URLUtils');
const Resource = require('dw/web/Resource');
const Site = require('dw/system/Site').getCurrent();

function getGenericMappingActionURL () {
    return {
        showPromoInformation: URLUtils.url('GenericMapping-ShowPromoInformation').toString(),
        errors: URLUtils.url('GenericMapping-Error').toString()
    }
}

function getData(email) {
    var MappingMgr = require('dw/util/MappingMgr');
    var MappingKey = require('dw/util/MappingKey');
    var mappingName = Site.getCustomPreferenceValue('mappingName') || 'promotion';

    var key = new MappingKey(email);
    var map = MappingMgr.get(mappingName, key);
    var warningMessage;
    var responceData;
    var pattern = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;

    if (!map && pattern.test(key.singleComponentKey)) {
        warningMessage = Resource.msg('form.email.not.found', 'main', null);
        responceData = {
            error: {
                message: warningMessage
            }
        }
    }

    if (map) {
        responceData = map.entrySet().toArray().reduce(function(acum,cur) {
            acum[cur.key] = cur.value
            return acum;
        }, {})
    }

    return responceData;
}

function GenericMappingModel (email) {
    this.genericMappingActionURL = getGenericMappingActionURL();
    this.responceData = getData(email);
}

module.exports = GenericMappingModel;


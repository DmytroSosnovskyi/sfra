const categories_el = document.querySelector('.js-categories');
const products_el = document.querySelector('.js-products');
const formEdit_el = document.querySelector('.js-form-edit');
const catalog_el = document.querySelector('.js-catalog');
let input_name = document.querySelector('.js-name');
let textarea_desc = document.querySelector('.js-description');
let textarea_details = document.querySelector('.js-details');
let popup = require('./popup');

// Category id.
var cgid;

// Boolean value. Idenfify is 'Edit' button or not.
let isEditButton;

// Use for change product name, description and details
var productId;

formEdit_el.onsubmit = e => {
    e.preventDefault();
    let url = catalog_el.getAttribute('data-action');
    fetch(url, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body: JSON.stringify({
            name: input_name.value,
            desc: textarea_desc.value,
            details: textarea_details.value,
            pid: productId
        })
    })
    .then(res => res.json())
    .then(data => {
        if (data.status === 'OK') {
            productItemHandler(cgid, true);
            initData(cgid);
            popup(data.text, 'lightskyblue');
        } else {
            popup(data.text, 'red');
        }
    })
}

/**
 * Gets data from controller 'Services-GetData'.
 * @param {*} cgid Name of category. Uses for getting products.
 */
const initData = cgid => {
    let url;
    if (!categories_el.getAttribute('is-categories-display')) {
        url = `${categories_el.getAttribute('data-action')}?sid=${categories_el.getAttribute('data-service-id')}`
    } else {
        if (cgid) {
            url = `${products_el.getAttribute('data-action')}?sid=${products_el.getAttribute('data-service-id')}&cgid=${cgid}`
        }
    }

    fetch(url)
    .then(res => res.json())
    .then(data => {
        if (data['_type'] === 'category') {
            displayCategories(data.categories, categories_el);
            return data.hasSpecificRole;
        }
        if (data['_type'] === 'product_search_result') {
            displayProducts(data.hits);
            return data.hasSpecificRole;
        }
    })
    .then(hasSpecificRole => {
        isEditButton = hasSpecificRole;
    })
}

initData();

/**
 * Creates and shows product items in dom
 * @param {*} products product hints
 */
const displayProducts = products => {
    let dom_el = document.createElement('div');
    dom_el.classList.add('product_items');

    products.forEach(product => {
        // Represents array of product information
        let productInfo = [
            product.product_name,
            product.price + product.currency,
        ]

        if (product.c_variations || product.c_selected_variations) {
            let variations = product.c_variations ? product.c_variations : product.c_selected_variations;
            productInfo.push({
                variations: variations
            })
        }

        if (dom_el.childNodes.length >= 5) {
            products_el.append(dom_el);
            dom_el = document.createElement('div');
            dom_el.classList.add('product_items');
        }

        // Represents a product hint in dom
        let product_item = document.createElement('div');
        product_item.classList.add('product_item');
        product_item.setAttribute('product_id', product.product_id);

        let img = new Image(200,200);
        img.src = product.image.link;
        product_item.append(img);

        //Add a button Edit if customer has specific role(like Administrator)
        if (isEditButton.value) {
            var edit_button = document.createElement('button');
            edit_button.innerHTML = 'Edit';
            edit_button.classList.add('btn','btn-info', 'btn-edit');
            edit_button.setAttribute('data-toggle','modal');
            edit_button.setAttribute('data-target', `#${editModal.getAttribute('id')}`);
            product_item.append(edit_button);

            edit_button.onclick = ('click', e => {
               // Use for change product name, description and details
                productId = e.target.parentElement.getAttribute('product_id');

                input_name.value = product.product_name;
                textarea_desc.value = product.c_description;
                textarea_details.value = product.c_details;
            })
        }

        // Array throuth productInfo. Adds product information to product_item
        for (let i = 0; i < productInfo.length; i++) {
            if (productInfo[i].variations) {
                productInfo[i].variations.forEach(variation => {
                    let product_item_el = document.createElement('span');
                    product_item_el.innerHTML = variation.name + ': ' + variation.value;
                    product_item.append(product_item_el);
                })
                break;
            }

            let product_item_el = document.createElement('span');
            product_item_el.innerHTML = productInfo[i];
            product_item.append(product_item_el);
        }

        dom_el.append(product_item);
    })
        products_el.append(dom_el);
}

/**
 * Creates and shows a navigation category tree
 * @param {*} categories list of categories
 * @param {*} rootEl html root element of each category
 */

const displayCategories = (categories,rootEl) => {
    categories.forEach(category => {
        let dom_el = document.createElement('div');
        let count_el = document.createElement('span');
        count_el.classList.add('count');
        count_el.innerHTML = `products: ${category.c_count}`;
        if (category.parent_category_id === 'root') {
            dom_el.classList.add(category.id, 'category__root');
        } else {
            dom_el.classList.add(category.id, 'category');
        }
        dom_el.innerHTML = category.name;
        if (category.categories) {
            // Sign has category a subcategory or not
            dom_el.classList.add('active');
            let dom_container = document.createElement('div');
            dom_container.classList.add(`${category.id}_items`, 'd-none');
            dom_el.append(dom_container);

            displayCategories(category.categories, dom_container);
        } else {
            // helps to display products through ocapi, by adding category.id to the request link
            dom_el.setAttribute('data-id', category.id);
        }
        dom_el.append(count_el);
        rootEl.append(dom_el);
    })
    categories_el.setAttribute('is-categories-display', true);
    categoryHandler(categories_el);
}


/**
 * Handles product item.
 * @param {*} cgid - Category id.
 * @param {*} deleteProducts Boolean value. Helps to delete products without dependings to category.
 * @returns {Boolean}
 */

const productItemHandler = (cgid, deleteProducts) => {
    if (products_el.getAttribute('data-category') !== cgid || deleteProducts) {
        while (products_el.firstChild) {
            products_el.firstChild.remove()
        }
        return true;
    } else {
        return false;
    }
}

/**
 * Adds ability to open each category by click.
 * @param {*} ancestor using for creat even delegation
 */

const categoryHandler = ancestor => {
    ancestor.onclick = ('click', e => {
        let el = e.target.closest('div');
        cgid = el.getAttribute('data-id');
        if (el.childNodes.length > 2 && el !== ancestor) {
            if (el.classList.contains('active') && el !== ancestor) {
                el.classList.remove('active');
            } else {
                el.classList.add('active');
            }
            if (el.childNodes[1].classList.contains('d-none')) {
                el.childNodes[1].classList.remove('d-none');
            } else {
                el.childNodes[1].classList.add('d-none');
            }
        }


        if (cgid) {
            if (productItemHandler(cgid)) {
                // data-category attribute helps to control prodcut vizialization.
                products_el.setAttribute('data-category', cgid);
                initData(cgid);
            }
        }

    })
}

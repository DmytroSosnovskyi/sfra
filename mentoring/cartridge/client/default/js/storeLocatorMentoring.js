const js_btn_group = document.querySelector('.js-btn-group');
const js_input = document.querySelector('.js-input');
const js_form_control_label = document.querySelector('.js-form-control-labal');
const js_form = document.querySelector('.store-locator');

js_form.onsubmit = e => {
    js_input.value = '';
}

js_btn_group.onclick = e => {
    let el = e.target.closest('label');
    js_input.setAttribute('name', el.getAttribute('value'));
    js_form_control_label.innerHTML = el.innerHTML;
    for (let i = 0; i < e.target.closest('div').childNodes.length; i++) {
        if (e.target.closest('div').childNodes[i].classList) {
            if (e.target.closest('div').childNodes[i].className === 'active' || el.className !== 'btn-secondary') {
                e.target.closest('div').childNodes[i].classList.add('btn-secondary');
                e.target.closest('div').childNodes[i].classList.remove('active');
                el.classList.remove('btn-secondary');
                el.classList.add('active');
            }
        }
    }
}
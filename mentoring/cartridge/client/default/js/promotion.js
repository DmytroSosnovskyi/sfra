const $form = $('.js-emailForm');

function isValidEmail (value) {
    var pattern = /^[\w.%+-]+@[\w.-]+\.[\w]{2,6}$/;
        return pattern.test(value);
}

$form.submit(function(e) {
    var $formInput = $('.js-emailForm-input');
    var $formError = $('.js-emailForm-error');

    $formError.empty();

    if (!isValidEmail($formInput.val()) || !$formInput.val()) {
        e.preventDefault();
        $.ajax({
            url: $form.attr('data-error'),
            method: 'GET',
            dataType: 'Json',
            data: $form.serialize(),
            success: data => {
                if (!isValidEmail($formInput.val())) {
                    $formError.text(data.notValid).addClass('text-danger');
                }
                if (!$formInput.val()) {
                    $formError.text(data.empty).addClass('text-danger');
                }
            },
            error: err => {
                if (!isValidEmail($formInput.val())) {
                    $formInput.addClass('is-invalid');
                }
                if (!$formInput.val()) {
                    $formInput.addClass('is-invalid');
                }
            }
        })
    }
})
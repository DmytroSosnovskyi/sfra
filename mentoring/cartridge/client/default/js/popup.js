const popup = (text, color) => {
    let dom_el = document.createElement('div');
    let el_text = document.createElement('span');
    dom_el.classList.add('popup');
    el_text.classList.add('popup__text');
    el_text.style.background = color;
    el_text.innerHTML = text;
    dom_el.append(el_text);
    document.body.append(dom_el);

    setInterval(() => {
        dom_el.remove();
    },5000)
}

module.exports = popup;
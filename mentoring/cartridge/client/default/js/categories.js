const categories_el = document.querySelector('.js-categories');
const products_el = document.querySelector('.js-products');
const formEdit_el = document.querySelector('.js-form-edit');
const input_name = document.querySelector('.js-name');
const textarea_desc = document.querySelector('.js-description');
const textarea_details = document.querySelector('.js-details');


// Access token from data API authorization.
var data_access_token;

// Boolean value. Idenfify has a customer specific role or not.
var hasSpecificRole;

// Use for change product name, description and details
var productId;

// Represents list of parameters for ocapi requests.
var ocapiParams = window.ocapi;

formEdit_el.onsubmit = ('submit', e => {
    e.preventDefault();

    updateProduct(input_name.value, textarea_desc.value, textarea_details.value);
})

/**
 * Updates products through data API
 * @param {*} name product name to change
 * @param {*} desc product description to change
 * @param {*} details product details to change
 */
const updateProduct = (name, desc , details) => {
    fetch(ocapiParams.updateProduct.replace('{{PID}}',productId), {
        method: 'PATCH',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${data_access_token}`
        },
        body: JSON.stringify({
            name: {
                default: name
            },
            short_description: {
                default: {
                    _type: 'markup_text',
                    markup: desc,
                    source: desc
                }
            },
            long_description: {
                default: {
                    _type: 'markup_text',
                    markup: details,
                    source: details
                }
            },
        })
    })
}

/**
 * Authorization by data API.
 * @param {*} email Email of a current customer.
 */
const dataApiAuth = email => {
    fetch(ocapiParams.auth_data, {
        method: "POST",
        mode: 'cors',
        headers: {
            'Content-Type': 'application/x-www-form-urlencoded;charset=UTF-8',
            'Authorization': 'Basic ' + window.btoa(ocapiParams.credentials_data)
        },
        body:  'grant_type=client_credentials'
    })
    .then(res => res.json())
    .then(data => {
        getCustomerRoles(email, data.access_token);
        return data.access_token;
    })
    .then(access_token => {
        data_access_token = access_token;
    })
}

/**
 * Verify has customer specific role or not
 * @param {*} email Email of a current customer.
 * @param {*} access_token Access token from data API authorization
 */
const getCustomerRoles = (email,access_token) => {
    fetch(ocapiParams.roles, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': `Bearer ${access_token}`
        },
        body: JSON.stringify({
            query: {
                text_query: {
                    fields: [
                        'email'
                    ],
                    search_phrase: email
                }
            },
            select: '(**)'
        })
    })
    .then(res => res.json())
    .then(data => data.count ? true : false)
    .then(isCount => {
        hasSpecificRole = isCount;
    })
}

/**
 * Handles product item
 * @param {*} categoryName
 * @returns {function}
 */

const productItemHandler = categoryName => {
    if (products_el.getAttribute('data-category') !== categoryName) {
        while (products_el.firstChild) {
            products_el.firstChild.remove()
        }
        return true;
    } else {
        return false;
    }
}


/**
 * Creates and shows product items in dom
 * @param {*} products product hints
 */
const initProducts = products => {
    let dom_el = document.createElement('div');
    dom_el.classList.add('product_items');

    products.forEach(product => {
        // Represents array of product information
        let productInfo = [
            product.product_name,
            product.price + product.currency,
        ]

        if (product.c_variations || product.c_selected_variations) {
            let variations = product.c_variations ? product.c_variations : product.c_selected_variations;
            productInfo.push({
                variations: variations
            })
        }


        if (dom_el.childNodes.length >= 5) {
            products_el.append(dom_el);
            dom_el = document.createElement('div');
            dom_el.classList.add('product_items');
        }

        // Represents a product hint in dom
        let product_item = document.createElement('div');
        product_item.classList.add('product_item');
        product_item.setAttribute('product_id', product.product_id);

        let img = new Image(200,200);
        img.src = product.image.link;
        product_item.append(img);

        // Add a button Edit if customer has specific role(like Administrator)
        if (hasSpecificRole) {
            var edit_button = document.createElement('button');
            edit_button.innerHTML = 'Edit';
            edit_button.classList.add('btn','btn-info', 'btn-edit');
            edit_button.setAttribute('data-toggle','modal');
            edit_button.setAttribute('data-target', `#${editModal.getAttribute('id')}`);
            product_item.append(edit_button);

            edit_button.onclick = ('click', e => {
                productId = e.target.parentElement.getAttribute('product_id');

                input_name.value = product.product_name;
                textarea_desc.value = product.c_description;
                textarea_details.value = product.c_details;
            })
        }

        // Array throuth productInfo. Adds product information to product_item
        for (let i = 0; i < productInfo.length; i++) {
            if (productInfo[i].variations) {
                productInfo[i].variations.forEach(variation => {
                    let product_item_el = document.createElement('span');
                    product_item_el.innerHTML = variation.name + ': ' + variation.value;
                    product_item.append(product_item_el);
                })
                break;
            }

            let product_item_el = document.createElement('span');
            product_item_el.innerHTML = productInfo[i];
            product_item.append(product_item_el);
        }

        dom_el.append(product_item);
    })
        products_el.append(dom_el);
}



/**
 * Creates and shows a navigation category tree
 * @param {*} categories list of categories
 * @param {*} rootEl html root element of each category
 */

const initCategories = (categories, rootEl) => {
    categories.forEach(category => {
        let dom_el = document.createElement('div');
        let count_el = document.createElement('span');
        count_el.classList.add('count');
        count_el.innerHTML = `products: ${category.c_count}`;
        if (category.parent_category_id === 'root') {
            dom_el.classList.add(category.id, 'category__root');
        } else {
            dom_el.classList.add(category.id, 'category');
        }
        dom_el.innerHTML = category.name;
        if (category.categories) {
            // Sign has category a subcategory or not
            dom_el.classList.add('active');
            let dom_container = document.createElement('div');
            dom_container.classList.add(`${category.id}_items`, 'd-none');
            dom_el.append(dom_container);

            initCategories(category.categories, dom_container);
        } else {
            // helps to display products through ocapi, by adding category.id to the request link
            dom_el.setAttribute('data-id', category.id);
        }
        dom_el.append(count_el);
        rootEl.append(dom_el);
    })
}


/**
 * Get data from given ocapi resource
 * @param {*} bearerToken
 */

const getData = (requestUrl, bearerToken, categoryName) => {
    let url;
    if (categoryName) {
        url = requestUrl + categoryName;
    } else {
        url = requestUrl;
    }

    fetch(url, {
        headers: {
            'Content-Type': 'application/json',
            'Authorization': bearerToken
        }
    })
    .then(res => {
        return res.json();
    })
    .then(data => {
        if (data['_type'] === 'category') {
            initCategories(data.categories,categories_el);
        }
        if (data['_type'] === 'product_search_result') {
            initProducts(data.hits);
        }
    })
}

/**
 * Adds ability to open each category by click.
 * @param {*} ancestor using for creat even delegation
 * @param {*} bearerToken using for getting products form ocapi.
 */

const categoryHandler = (ancestor, bearerToken) => {
    ancestor.onclick = ('click', e => {
        let el = e.target.closest('div');
        if (el.childNodes.length > 2 && el !== ancestor) {
            if (el.classList.contains('active') && el !== ancestor) {
                el.classList.remove('active');
            } else {
                el.classList.add('active');
            }
            if (el.childNodes[1].classList.contains('d-none')) {
                el.childNodes[1].classList.remove('d-none');
            } else {
                el.childNodes[1].classList.add('d-none');
            }
        }


        if (el.getAttribute('data-id')) {
              // data-category attribute helps to control prodcut vizialization
            if (productItemHandler(el.getAttribute('data-id'))) {
                products_el.setAttribute('data-category', el.getAttribute('data-id'));
                getData(ocapiParams.getProducts, bearerToken, el.getAttribute('data-id'));
            }
        }

    })
}


/**
 * Initializes module
 */

const init = () => {
    // Makes customer auth
    fetch(ocapiParams.auth, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            'Authorization': 'Basic ' + window.btoa(ocapiParams.credentials)
        },
        body: JSON.stringify({
            type: 'credentials'
        })
    })
    .then(res => {
        getData(ocapiParams.getCategories, res.headers.get('authorization'));
        categoryHandler(categories_el, res.headers.get('authorization'));
        return res.json()
    })
    .then(data => {
        if (data.auth_type === 'registered') {
            dataApiAuth(data.email);
        }
    })
}


init();

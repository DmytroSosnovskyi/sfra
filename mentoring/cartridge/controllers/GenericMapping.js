'use strict'
var server = require('server');
var csrfProtection = require('*/cartridge/scripts/middleware/csrf');

const GenericMapplngModel = require('*/cartridge/models/genericMappingModel');
const Resource = require('dw/web/Resource');

// Entry point 'Show', render genericMapping template with action URL and customer email in pdict.

server.get(
    'Show',
    server.middleware.https,
    csrfProtection.generateToken,
    function (req,res,next) {
        var emailForm = server.forms.getForm('email');
        emailForm.clear();

        const genericMapplngModel = new GenericMapplngModel();
        var action = genericMapplngModel.genericMappingActionURL.showPromoInformation;
        var errors = genericMapplngModel.genericMappingActionURL.errors;

        if (customer.isRegistered()) {
            var customerEmail = customer.getProfile().email;
        }

        res.render('emailFormTemplate', {
            emailForm: emailForm,
            errors: errors,
            action: action,
            customerEmail: customerEmail || ''
        });
        next();
})

// Entry point 'ShowPromoInformation' are getting data form genericMappingModel.

server.post('ShowPromoInformation',
    function(req,res,next) {
        var emailForm = server.forms.getForm('email');
        const genericMapplngModel = new GenericMapplngModel(emailForm.email.htmlValue);
        var responce = genericMapplngModel.responceData;

        if (responce.error) {
            emailForm.valid = false;
        }

        if (emailForm.valid) {
            res.render('result', {
                data: responce,
                success: true,
            })
        } else {
            res.render('result',{
                data: responce,
                success: false
            })
        }

        next();
})

// Json with list of error resources.

server.get('Error',
    function (req,res,next) {
        var errors = {
            empty: Resource.msg('form.missing.error', 'main', null),
            notValid: Resource.msg('form.email.not.valid', 'main', null)
        }

        res.json(errors);

        next();
})

module.exports = server.exports();
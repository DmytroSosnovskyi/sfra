var server = require('server');
var storeHelpers = require('*/cartridge/scripts/helpers/storeHelpers');

var page = module.superModule;
server.extend(page);

// Finds stores by an approperly query.
server.append('FindStores', function (req,res,next) {
    var hm = request.httpParameterMap;
    var viewData = res.getViewData();
    var query = storeHelpers.getQueryValue(hm);
    var radius = req.querystring.radius;
    var postalCode = req.querystring.postalCode;
    var lat = req.querystring.lat;
    var long = req.querystring.long;
    var showMap = req.querystring.showMap || false;
    var url = '';

    var stores = storeHelpers.getStores(radius, postalCode, lat, long, req.geolocation, showMap, url, query);
    res.json(stores);
    next();
})

module.exports = server.exports();
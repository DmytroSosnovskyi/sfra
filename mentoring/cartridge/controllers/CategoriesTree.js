'use strict'
var server = require('server');

/**
 * Renders a categiryTree isml template
 */

server.get('Show',
    function(req,res,next) {
        var editProductForm = server.forms.getForm('editProduct');
        editProductForm.clear();

        res.render('categoryTree', {
            editProductForm: editProductForm
        });

        next();
    }
)

module.exports = server.exports();
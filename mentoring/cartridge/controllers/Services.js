'use strict'
var server = require('server');
const Auth = require('*/cartridge/scripts/helpers/auth');
const getData = require('*/cartridge/scripts/helpers/getData');
const ServiceUrls = require('*/cartridge/scripts/helpers/serviceUrls');
const Site = require('dw/system/Site').getCurrent();
const CacheMgr = require('dw/system/CacheMgr');
const getCategoriesServiceId = Site.getCustomPreferenceValue('Services_getCategories') || 'getCategories';
const getProductsServiceId = Site.getCustomPreferenceValue('getProduct') || 'getProduct';
const Resource = require('dw/web/Resource');
var serviceCache = CacheMgr.getCache('serviceCache');
var accessToken = serviceCache.get('access_token');

// Renders categoryTreeService template. Pass servcices id and urls for requests on client.
server.get('Show',
    function(req,res,next) {
        var editProductForm = server.forms.getForm('editProduct');
        editProductForm.clear();

        res.render('categoryTreeService', {
            editProductForm: editProductForm,
            getData: ServiceUrls.getData,
            getCategoriesServiceId: getCategoriesServiceId,
            getProductsServiceId: getProductsServiceId,
            updateProduct: ServiceUrls.updateProduct
        });
        next();
    }
)

// Gets data from ocapi through services. Data depends on querystring(categories or products).
server.get('GetData',
    function(req,res,next) {
        var data;
        try {
            data = getData(accessToken.value, req.querystring.sid, req.querystring.cgid).call({});
        } catch (err) {
            Auth.call({});
            accessToken = serviceCache.get('access_token');
            data = getData(accessToken.value, req.querystring.sid, req.querystring.cgid).call({});
        }
        data.object['hasSpecificRole'] = serviceCache.get('hasSpecificRoles');
        res.json(data.object);
        next()
    }
)


// Updates product through ocapi by using service.
server.post('UpdateProduct',
    function(req,res,next) {
        const ProductUpdate = require('*/cartridge/scripts/helpers/productUpdates');
        var body;
        try {
            body = JSON.parse(req.body);
        } catch (err) {
            body = req.body;
        }

        var productUpdateService = ProductUpdate(body.name, body.desc, body.details, body.pid).call({});

        if (productUpdateService.status === 'OK') {
            res.json({
                status: productUpdateService.status,
                text: Resource.msg('popup.success', 'main', null)
            })
        } else {
            res.json({
                status: productUpdateService.status,
                text: Resource.msg('popup.not.success', 'main', null)
            })
        }
        next();
    }
)


module.exports = server.exports();
'use strict';

/**
 * Returns array of stores that contain an appropely product.
 * @param {*} storeMgrResult Set of stores.
 * @param {*} productId Product Id.
 * @returns {Array}
 */

function getStoresByProductId (storeMgrResult, productId) {
    var ProductInventoryMgr = require('dw/catalog/ProductInventoryMgr');
    var productInventoryList = storeMgrResult.toArray().filter(function(store) {
        return store.inventoryListID !== null;
    }).map(function(store) {
        return ProductInventoryMgr.getInventoryList(store.inventoryListID)
    }).filter(function(productInventoryList) {
        return productInventoryList.getRecord(productId) ? productInventoryList.getRecord(productId).stockLevel.available : false;
    });

    return storeMgrResult.toArray().filter(function(store) {
        var matchedStore;
        productInventoryList.forEach(function(productInventory) {
            if (productInventory.ID === store.inventoryListID) {
                matchedStore = store;
            }
        })
        return matchedStore;
    });
}

/**
 * Returns array of stores that contain an appropely phone number.
 * @param {*} storeMgrResult Set of stores.
 * @param {*} phone Phone number.
 * @returns {Array}
 */

function getStoresByPhone (storeMgrResult, phone) {
    return storeMgrResult.toArray().filter(function(store) {
        return store.phone === phone;
    })
}

/**
 * Searches for stores and creates a plain object of the stores returned by the search
 * @param {string} radius - selected radius
 * @param {string} postalCode - postal code for search
 * @param {string} lat - latitude for search by latitude
 * @param {string} long - longitude for search by longitude
 * @param {Object} geolocation - geloaction object with latitude and longitude
 * @param {boolean} showMap - boolean to show map
 * @param {dw.web.URL} url - a relative url
 * @returns {Object} a plain object containing the results of the search
 */
function getStores(radius, postalCode, lat, long, geolocation, showMap, url, query) {
    var StoresModel = require('*/cartridge/models/stores');
    var StoreMgr = require('dw/catalog/StoreMgr');
    var Site = require('dw/system/Site');
    var URLUtils = require('dw/web/URLUtils');

    var countryCode = geolocation.countryCode === 'US' ? geolocation.countryCode : 'US';
    var distanceUnit = countryCode === 'US' ? 'mi' : 'km';
    var resolvedRadius = radius ? parseInt(radius, 10) : 15;

    var searchKey = {};
    var storeMgrResult = null;
    var location = {};
    var actionUrl = url || URLUtils.url('Stores-FindStores', 'showMap', showMap).toString();
    var apiKey = Site.getCurrent().getCustomPreferenceValue('mapAPI');

    if (postalCode && postalCode !== '') {
        // find by postal code
        searchKey = postalCode;
        storeMgrResult = StoreMgr.searchStoresByPostalCode(
            countryCode,
            searchKey,
            distanceUnit,
            resolvedRadius
        );
        searchKey = { postalCode: searchKey };
    } else if (query) {
        // find by name, phone, city, address1, address2, productId
        resolvedRadius = Infinity;
        if (query.name === 'productId' || query.name === 'phone') {
            storeMgrResult = StoreMgr.searchStoresByPostalCode(
                countryCode,
                '10001',
                distanceUnit,
                resolvedRadius
            ).keySet();

            if (query.name === 'productId') {
                storeMgrResult = getStoresByProductId(storeMgrResult, query.value);
            } else {
                storeMgrResult = getStoresByPhone(storeMgrResult, query.value);
            }

            searchKey = { postalCode: '10001' };
            var searchByAdditionalQuery = true;
            return new StoresModel(storeMgrResult, searchKey, resolvedRadius, actionUrl, apiKey, searchByAdditionalQuery);
        } else {
            storeMgrResult = StoreMgr.searchStoresByPostalCode(
                countryCode,
                '10001',
                distanceUnit,
                resolvedRadius,
                query.name + ' ILIKE {0}',
                query.value
            );
            searchKey[query.name] = query.value;
        }
    } else {
        // find by coordinates (detect location)
        location.lat = lat && long ? parseFloat(lat) : geolocation.latitude;
        location.long = long && lat ? parseFloat(long) : geolocation.longitude;

        storeMgrResult = StoreMgr.searchStoresByCoordinates(location.lat, location.long, distanceUnit, resolvedRadius);
        searchKey = { lat: location.lat, long: location.long };
    }

    var stores = new StoresModel(storeMgrResult.keySet(), searchKey, resolvedRadius, actionUrl, apiKey);

    return stores;
}

/**
 * create the stores results html
 * @param {Array} storesInfo - an array of objects that contains store information
 * @returns {string} The rendered HTML
 */
function createStoresResultsHtml(storesInfo, isInventory) {
    var HashMap = require('dw/util/HashMap');
    var Template = require('dw/util/Template');

    var context = new HashMap();

    if (isInventory) {
        var object = { stores: { stores: [storesInfo] } };
    } else {
        var object = { stores: { stores: storesInfo } };
    }

    Object.keys(object).forEach(function (key) {
        context.put(key, object[key]);
    });

    var template = new Template('storeLocator/storeLocatorResults');
    return template.render(context).text;
}

/**
 * Returns query object for searching store.
 * @param {*} hm Http parameter map.
 * @returns {Object}
 */

function getQueryValue (hm) {
    var query = {};
    var queries = ['city', 'name', 'phone', 'address1','address2','productId'];
    for (let i = 0; i < queries.length; i++) {
        if (!hm.get(queries[i]).empty) {
            query = {
                name: queries[i],
                value: hm.get(queries[i]).value
            }
            break;
        }
    }

    return query;
}

module.exports = exports = {
    createStoresResultsHtml: createStoresResultsHtml,
    getStores: getStores,
    getQueryValue: getQueryValue
};

const Site = require('dw/system/Site').getCurrent();
const Urls = require('*/cartridge/scripts/helpers/urls');
const Logger = require('dw/system/Logger')
const Resource = require('dw/web/Resource');

/**
 * Get urls and credentials from site Preferences
 * @returns {Object}
 */

function getOcapiParams () {
    const LoginShop = Site.getCustomPreferenceValue('OCAPI_shop_login') || 'Dmytro_sosnovskyi@epam.com';
    const PasswordShop = Site.getCustomPreferenceValue('OCAPI_shop_password') || 'Test111$';
    const CredentialsShop = LoginShop + ':' + PasswordShop;
    const LoginData = Site.getCustomPreferenceValue('OCAPI_data_login') || 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
    const PasswordData = Site.getCustomPreferenceValue('OCAPI_data_password') || 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';
    const CredentialsData = LoginData + ':' + PasswordData;
    const Role = Site.getCustomPreferenceValue('OCAPI_role') || 'Administrator';
    const Client_ID = Site.getCustomPreferenceValue('OCAPI_clientID') || 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';

    var endPoints = ['auth','getCategories','getProducts','dataAuth','updateProduct'];
    var urls;

    try {
        urls = JSON.parse(Site.getCustomPreferenceValue('OCAPI_urls'));
        for (let i = 0; i < endPoints.length; i++) {
            if (typeof urls[endPoints[i]] !== 'string') {
                Logger.error(Resource.msg('error.not.valid.json', 'main', null));
                throw new Error(Resource.msg('error.not.valid.json', 'main', null));
            }
        }
    } catch (err) {
        urls = Urls;
    }

    return {
        getCategories: urls.getCategories,
        auth: urls.auth.replace('{{ID}}', Client_ID),
        getProducts: urls.getProducts,
        credentials: CredentialsShop,
        credentials_data: CredentialsData,
        auth_data: urls.dataAuth,
        roles: urls.roles.replace('{{role}}', Role),
        updateProduct: urls.updateProduct
    }
}

module.exports = {
    getOcapiParams: getOcapiParams
}
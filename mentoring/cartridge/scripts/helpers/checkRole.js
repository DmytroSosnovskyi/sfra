const LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
const Site = require('dw/system/Site').getCurrent();
const ServiceId = Site.getCustomPreferenceValue('Services_checkRole') || 'checkRole';
const CacheMgr = require('dw/system/CacheMgr');
const ServiceCache = CacheMgr.getCache('serviceCache');
const Role = Site.getCustomPreferenceValue('OCAPI_role') || 'Administrator';
const Resource = require('dw/web/Resource');

/**
 * Calls the service 'checkRole'. The servcie creates request to OCAPI end point role/{{role}}/user_search.
 * @param {*} access_token
 * @param {*} email
 * @returns {service}
 */
function checkRole (access_token,email) {
    if (access_token === undefined || access_token === null) {
        throw new SyntaxError(Resource.msg('error.access.token.not.valid', 'main', null));
    }

    var request = LocalServiceRegistry.createService(ServiceId, {
        createRequest: function (service,param) {
            service.addHeader('Content-Type', 'application/json');
            service.addHeader('Authorization', 'Bearer ' + access_token);
            service.setRequestMethod('POST');

            return JSON.stringify({
                query: {
                    text_query: {
                        fields: [
                            'email'
                        ],
                        search_phrase: email
                    }
                },
                select: '(**)'
            })
        },
        parseResponse: function(service,param) {
            var response;
            try {
                response = JSON.parse(param.text);
            } catch (err) {
                response = param.text;
            }
            ServiceCache.put('hasSpecificRoles', {
                value: response.count ? true : false
            });
        }
    });

    request.setURL(request.URL.replace('{{HOST}}', Site.httpHostName).replace('{{role}}', Role));

    return request;
}

module.exports = checkRole;


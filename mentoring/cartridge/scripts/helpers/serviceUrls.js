const URLUtils = require('dw/web/URLUtils');

// Exports end points urls for controller 'Service'.
module.exports = {
    getData: URLUtils.url('Services-GetData').toString(),
    updateProduct: URLUtils.url('Services-UpdateProduct').toString()
}
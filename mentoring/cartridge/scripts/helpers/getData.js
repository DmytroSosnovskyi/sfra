const LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
const Site = require('dw/system/Site').getCurrent();
const Resource = require('dw/web/Resource');

/**
 * Gets data from Ocapi (categories or products).
 * @param {*} accsessToken Token for authorization.
 * @param {*} serviceID Id of the service(getCategories or getProdcuts).
 * @param {*} categoryID Use for getting products through OCAPi.
 * @returns {service}
 */
function getData (accsessToken, serviceID, categoryID) {
    if (accsessToken === undefined || accsessToken === null) {
        throw new SyntaxError(Resource.msg('error.access.token.not.valid', 'main', null));
    }
    var request =  LocalServiceRegistry.createService(serviceID, {
        mockFull: function (servcice,param) {
            if (serviceID === 'getCategories') {
                return {
                    _type: 'category',
                    categories: [
                        {
                            c_count: 1,
                            parent_category_id: 'root',
                            id: 'top-seller',
                            name: 'Top Sellers'
                        }
                    ]
                }
            }
            if (serviceID === 'getProduct') {
                return {
                    _type: 'product_search_result',
                    hits: [
                        {
                            product_name: 'perfect skirt',
                            price: 79,
                            currency: 'USD',
                            c_variations: [
                                {
                                    name: 'Color',
                                    value: 'midnight navy,Chino'
                                },
                                {
                                    name: 'Size',
                                    value: '4,8,10,12,14,16'
                                }
                            ],
                            parent_category_id: 'root',
                            id: 'top-seller',
                            name: 'Top Sellers',
                            product_id: '25553417M',
                            image: {
                                link: 'https://zzrp-004.sandbox.us01.dx.commercecloud.salesforce.com/on/demandware.static/-/Sites-apparel-m-catalog/default/dw3087b6c5/images/large/PG.10239566.JJ0VWXX.PZ.jpg'
                            },
                            c_description: 'What a perfect skirt for business travel. Pack this mock wrap sleek knit dress in a suitcase, and it will pop out ready to wear.',
                            c_details: 'What a perfect skirt for business travel. Pack this mock wrap sleek knit dress in a suitcase, and it will pop out ready to wear.',
                        }
                    ]
                }
            }
        },
        createRequest: function (service, param) {
            service.addHeader('Content-Type', 'application/json');
            service.addHeader('Authorization', accsessToken);
            service.setRequestMethod('GET');
        },
        parseResponse: function(service,param) {
            var response;
            try {
                response = JSON.parse(param.text);
            } catch (err) {
                response = param.text;
            }
            return response;
        }
    })
    request.setURL(request.URL.replace('{{HOST}}', Site.httpHostName).replace('{{SiteID}}', Site.ID));
    if (categoryID) {
        request.setURL(request.URL.replace('{{cgID}}', categoryID));
    }
    return request;
}

module.exports = getData;
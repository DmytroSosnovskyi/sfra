const LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
const Site = require('dw/system/Site').getCurrent();
const Client_ID = Site.getCustomPreferenceValue('OCAPI_clientID')
const Encoding = require('dw/crypto/Encoding');
const Bytes = require('dw/util/Bytes');
const shopAuthServiceID = Site.getCustomPreferenceValue('Services_shop_auth') || 'shop_auth';
const dataAuthServiceID = Site.getCustomPreferenceValue('Services_data_auth') || 'data_auth';
const CacheMgr = require('dw/system/CacheMgr');
const CheckRole = require('*/cartridge/scripts/helpers/checkRole');
const serviceCache = CacheMgr.getCache('serviceCache');
var access_token = serviceCache.get('access_token_data');

// Authorization for data API.
var data_auth = LocalServiceRegistry.createService(dataAuthServiceID, {
    createRequest: function (service,param) {
        let credentials = service.configuration.credential.user + ':' + service.configuration.credential.password;
        service.addHeader('Content-Type', 'application/x-www-form-urlencoded;charset=UTF-8');
        service.addHeader('Authorization', 'Basic ' + Encoding.toBase64(Bytes(credentials)));
        service.setRequestMethod('POST');

        return 'grant_type=client_credentials';
    },
    parseResponse: function(service,param) {
        var response;
        try {
            response = JSON.parse(param.text);
        } catch (err) {
            response = param.text;
        }
        serviceCache.put('access_token_data', {
            value: response.access_token
        });
    }
})

// Authorization for shop API.
var shop_auth = LocalServiceRegistry.createService(shopAuthServiceID, {
    createRequest: function (service,param) {
        let credentials = service.configuration.credential.user + ':' + service.configuration.credential.password;
        service.addHeader('Content-Type', 'application/json');
        service.addHeader('Authorization', 'Basic ' + Encoding.toBase64(Bytes(credentials)));
        service.setRequestMethod('POST');

        return JSON.stringify({
            'type': 'credentials'
        });
    },
    parseResponse: function(service,param) {
        var response;
        try {
            response = JSON.parse(param.text);
        } catch (err) {
            response = param.text;
        };

        if (response.auth_type === 'registered') {
            try {
                CheckRole(access_token.value, response.email).call({});
            } catch (err) {
                data_auth.call({});
                access_token = serviceCache.get('access_token_data');
                CheckRole(access_token.value, response.email).call({});
            }
        };

        serviceCache.put('access_token', {
            value: param.responseHeaders['authorization']
        });
    }
})
shop_auth.setURL(
    shop_auth.URL
    .replace('{{HOST}}', Site.httpHostName)
    .replace('{{SiteID}}', Site.ID)
    .replace('{{ClientID}}', Client_ID)
);

module.exports = shop_auth;

const Site = require('dw/system/Site').getCurrent();
const HOST = Site.httpHostName;
const SITE_ID = Site.ID;
const Client_ID = Site.getCustomPreferenceValue('OCAPI_clientID') || 'aaaaaaaaaaaaaaaaaaaaaaaaaaaaaa';

const data_url = 'https://' + HOST + '/s/-/dw/data/v20_9';
const shop_url = 'https://' + HOST + '/s/{{SITE_ID}}/dw/shop/v20_9'.replace('{{SITE_ID}}', SITE_ID);

module.exports = {
    auth: shop_url + '/customers/auth?client_id=' + Client_ID,
    getCategories: shop_url + '/categories/root?levels=3',
    getProducts: shop_url + '/product_search?expand=images,prices,variations&refine=cgid=',
    dataAuth: 'https://account.demandware.com/dwsso/oauth2/access_token',
    roles: data_url + '/roles/{{role}}/user_search',
    updateProduct: data_url + '/products/{{PID}}'
}
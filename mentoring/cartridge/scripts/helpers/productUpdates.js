const LocalServiceRegistry = require('dw/svc/LocalServiceRegistry');
const Site = require('dw/system/Site').getCurrent();
const updateProductServiceId = Site.getCustomPreferenceValue('Services_updateProduct') || 'updateProduct';
const CacheMgr = require('dw/system/CacheMgr');
const serviceCache = CacheMgr.getCache('serviceCache');
var access_token = serviceCache.get('access_token_data');


/**
 * Updates product through OCAPI by using 'updateProduct' service.
 * @param {*} name The name of product.
 * @param {*} desc The description of product.
 * @param {*} details The details of product.
 * @param {*} pid The prodict ID.
 * @returns {service}
 */

function productUpdates (name, desc, details, pid) {
    var request = LocalServiceRegistry.createService(updateProductServiceId, {
        createRequest: function (service, param) {
            service.addHeader('Content-Type', 'application/json');
            service.addHeader('Authorization', 'Bearer ' + access_token.value);
            service.setRequestMethod('PATCH');

            return JSON.stringify({
                name: {
                    default: name
                },
                short_description: {
                    default: {
                        _type: 'markup_text',
                        markup: desc,
                        source: desc
                    }
                },
                long_description: {
                    default: {
                        _type: 'markup_text',
                        markup: details,
                        source: details
                    }
                }
            })
        },
        parseResponse: function(service,param) {}
    })

    request.setURL(request.URL.replace('{{HOST}}', Site.httpHostName).replace('{{pid}}', pid));

    return request;
}


module.exports = productUpdates;
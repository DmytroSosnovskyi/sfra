'use strict';
const ProductMgr = require('dw/catalog/ProductMgr');


/**
 * Updates product with new attributes
 * @param {*} hits Represent array with product
 */

function updateProduct (hits) {
    const Site = require('dw/system/Site').getCurrent();
    hits.toArray().forEach(function(hit) {
        let product = ProductMgr.getProduct(hit.productId);
        hit.c_description = product.shortDescription ? product.shortDescription.markup : '';
        hit.c_details = product.longDescription ? product.longDescription.markup: '';

        if (hit.productType.master) {
            let variationAttributes = product.variationModel.productVariationAttributes;
            let variations = variationAttributes.toArray().map(function(attribute) {
                let allValues = product.variationModel.getAllValues(attribute);
                return {
                    name: attribute.displayName,
                    value: allValues.toArray().map(function(value) {
                        return value.displayValue;
                    }).join(',')
                }
            })
            hit.c_variations =  variations;
        }
        if (hit.productType.variant) {
            var variationModel = product.variationModel.selectedVariant.variationModel;
            let variations = variationModel.productVariationAttributes.toArray().map(function(attribute) {
                return {
                    name: attribute.displayName,
                    value: variationModel.selectedVariant.custom[attribute.ID]
                }
            })
            hit.c_selected_variations = variations;
        }
    })
}

/**
 * Handles modifyGETResponse extension point
 * @param {*} products
 */
function modifyGETResponse (products) {
    updateProduct(products.hits);
}

exports.modifyGETResponse = modifyGETResponse;
const ProductSearchModel = require('dw/catalog/ProductSearchModel');
const searchHelper = require('*/cartridge/scripts/helpers/searchHelpers');

/**
 * Adds c_count attribute to each category
 * @param {*} categories list of categories
 */

function modifyCategory (categories) {
    categories.toArray().forEach(function(category) {
        let apiProductSearchModel = new ProductSearchModel();
        apiProductSearchModel = searchHelper.setupSearch(apiProductSearchModel, {
            cgid: category.id
        });
        apiProductSearchModel.search();
        category.c_count = apiProductSearchModel.count;

        if (category.categories !== null && category.categories.length > 0) {
            modifyCategory(category.categories);
        }

    })
}

/**
 * Handles modifyGETResponse extension point
 * @param {*} category
 * @param {*} doc
 */

function modifyGETResponse (category, doc) {
    modifyCategory(doc.categories);
}

exports.modifyGETResponse = modifyGETResponse;

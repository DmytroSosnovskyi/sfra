const Logger = require('dw/system/Logger').getLogger('custom.setTimeInterval');
const Status = require('dw/system/Status');
const Calendar = require('dw/util/Calendar');


function run (parameters, stepExecution) {
  var pattern = /^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;

  var startTime = parameters.startTime;
  var endTime = parameters.endTime;
  var date = new Calendar();
  var jobDateStart = new Calendar();
  var jobDateEnd = new Calendar();
  var year = date.getTime().getFullYear();
  var month = date.getTime().getMonth();
  var day = date.getTime().getDate();


  var startHours
  var endHours;
  var startMinutes;
  var endMinutes;

  if (pattern.test(startTime)) {
    startHours = +startTime.split(':')[0];
    startMinutes = +startTime.split(':')[1];

  } else {
    Logger.error('Start time parameter is not valid')
    return new Status(Status.ERROR);
  }

  if (pattern.test(endTime)) {
    endHours = +endTime.split(':')[0];
    endMinutes = +endTime.split(':')[1];
  } else {
    Logger.error('End time parameter is not valid');
    return new Status(Status.ERROR);
  }

  jobDateStart.set(year,month,day,startHours,startMinutes);
  jobDateEnd.set(year,month,day,endHours,endMinutes);

  if (jobDateStart.compareTo(date) <= 0 && jobDateEnd.compareTo(date) > 0) {
    return new Status(Status.OK);
  } else {
    return new Status(Status.OK, 'NOT_VALID_TIME')
  }
}

exports.Run = run;
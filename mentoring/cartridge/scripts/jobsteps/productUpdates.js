const File = require('dw/io/File');
const Status = require('dw/system/Status');
const Logger = require('dw/system/Logger').getLogger('custom.productUpdates');;
const XMLStreamReader = require('dw/io/XMLStreamReader');
const FileReader = require('dw/io/FileReader');
const XMLStreamConstants = require('dw/io/XMLStreamConstants');
const XMLStreamWriter = require('dw/io/XMLStreamWriter');
const FileWriter = require('dw/io/FileWriter')
const ProductMgr = require('dw/catalog/ProductMgr');
const Transaction = require('dw/system/Transaction');


var xmlStreamReader;
var xmlStreamWriter;
// boolean variable, controls are products update through XML
var throughXML = false;
// boolean variable, controls are products update through API
var throughAPI = false;

/**
 * The handleProducts adding new custom attribute to the product. Checkbox dicedes either through XML file or by direct through API.
 * @param {*} products is array of object, contains product id and product varationAttributes.
 * @param {*} createXMLFile boolean variable, decides how to add attribute to the product.
 * @param {*} targetFile represents a target XML file.
 * @param {*} masterCatalog represents master catalog
 */

function handleProducts (products, createXMLFile, targetFile, masterCatalog) {
    if (createXMLFile) {
        var fileWriter = new FileWriter(targetFile, 'UTF-8');
        xmlStreamWriter = new XMLStreamWriter(fileWriter);

        xmlStreamWriter.writeStartDocument();
        xmlStreamWriter.writeStartElement('catalog');
        xmlStreamWriter.writeAttribute('xmlns','http://www.demandware.com/xml/impex/catalog/2006-10-31');
        xmlStreamWriter.writeAttribute('catalog-id', masterCatalog);
        products.forEach(function(product) {
            xmlStreamWriter.writeStartElement('product');
            xmlStreamWriter.writeAttribute('product-id',product.id);
            xmlStreamWriter.writeStartElement('custom-attributes');
            xmlStreamWriter.writeStartElement('custom-attribute');
            xmlStreamWriter.writeAttribute('attribute-id', 'variationAttributes');
            xmlStreamWriter.writeCharacters(JSON.stringify(product.variationAttributes));
            xmlStreamWriter.writeEndElement();
            xmlStreamWriter.writeEndElement();
            xmlStreamWriter.writeEndElement();
        })
        xmlStreamWriter.writeEndElement();
        xmlStreamWriter.writeEndDocument();
        xmlStreamWriter.close();
        fileWriter.close();

        throughXML = true;
    } else {
        products.forEach(function(product) {
            var productEl = ProductMgr.getProduct(product.id);
            if (productEl) {
                Transaction.wrap(function() {
                    productEl.custom.variationAttributes = JSON.stringify(product.variationAttributes);
                })
            }
        })
        throughAPI = true;
    }
}

function run (parameters,stepExecutition) {
    var masterCatalog;
    var targetSourceFile = new File(parameters.sourceFolderPath + File.SEPARATOR + parameters.sourceFileName);
    var customAttributes = ['color','size','width','length'];
     // represent array of object with product-id and variationAttributes
    var products = [];
     // Maximum length of products array
    var quotaLimit = 19999;
     // Help to create products array
    var index = 0;
    var targetFile;
    var fileReader = new FileReader(targetSourceFile,'UTF-8');

    if (parameters.createXMLFile) {
        targetFile = new File(parameters.targetFolderPath + File.SEPARATOR + parameters.targetFileName + '.xml');
    }
    if (!targetSourceFile.exists()) {
        Logger.error('Target source file does not exist');
        return new Status(Status.ERROR);
    }
    if (!targetSourceFile.isFile()) {
        Logger.error('Target source file is not a file');
        return new Status(Status.ERROR);
    }

    xmlStreamReader = new XMLStreamReader(fileReader);

    // Filling products array by given xml file
    while(xmlStreamReader.hasNext()) {
        var xmlStreamReaderNext = xmlStreamReader.next();
        var elementName;
        if (xmlStreamReaderNext === XMLStreamConstants.START_ELEMENT) {
            elementName = xmlStreamReader.getLocalName();
            if (elementName === 'catalog') {
                masterCatalog = xmlStreamReader.getAttributeValue(0);
            }
            if (elementName === 'product') {
                if (products.length >= quotaLimit) {
                    handleProducts(products,parameters.createXMLFile,targetFile,masterCatalog);
                    index = 0;
                    products.length = 0;
                }
                products[index] = {
                    id: xmlStreamReader.getAttributeValue(0),
                    variationAttributes: {}
                }
            }
            if (elementName === 'custom-attribute') {
                    var attributeName = xmlStreamReader.getAttributeValue(0);
                    if (customAttributes.indexOf(attributeName) >= 0) {
                        products[index].variationAttributes[attributeName] =  xmlStreamReader.getElementText();
                }
            }
        }
        if (xmlStreamReaderNext === XMLStreamConstants.END_ELEMENT) {
            elementName = xmlStreamReader.getLocalName();
            if (elementName === 'product') {
                if (!Object.keys(products[products.length - 1].variationAttributes).length) {
                    products.pop();
                } else {
                    index++;
                }
            }
        }
    }

    if (!products.length) {
        Logger.debug('Has no products for update');
        return new Status(Status.OK, 'HAS_NO_PRODUCTS_FOR_UPDATE');
    }
    handleProducts(products,parameters.createXMLFile,targetFile,masterCatalog);
    if (throughAPI) {
        return new Status(Status.OK, 'UPDATES_SUCCESS');
    }
    if (throughXML) {
        return new Status(Status.OK);
    }
}

exports.Run = run;

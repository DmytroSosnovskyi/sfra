const FTPClient = require('dw/net/FTPClient');
const File = require('dw/io/File');
const Logger = require('dw/system/Logger').getLogger('custom.uploadExportProducts');
const Status = require('dw/system/Status');
const Site = require('dw/system/Site').getCurrent();

function run (parameters, stepExecution) {
  var targetFolder = new File(parameters.targetFolder);

  if (!targetFolder.exists()) {
    Logger.error('Source direcroty does not exist.');
    return new Error(Status.ERROR, 'DIRECTORY_NOT_EXIST');
  }
  if (!targetFolder.isDirectory()) {
    Logger.error('Source directory doen not a directory');
    return new Error(Status.ERRPR, 'DIRECTORY_NOT_VALID')
  }
  var filesList = targetFolder.listFiles().toArray().filter(function(file) {
    return !file.isDirectory() && file.getName().match(parameters.fileName);
  })
  if (!filesList.length) {
    Logger.error('Source directory does not contain upload files');
    return new Status(Status.OK, 'NO_FILES_FOUND');
  }

  var ftpClient = new FTPClient();
  var host = Site.getCustomPreferenceValue('FTP_host');
  var userName = Site.getCustomPreferenceValue('FTP_userName');
  var password = Site.getCustomPreferenceValue('FTP_password');
  var timeOut = Site.getCustomPreferenceValue('FTP_timeOut') || 30000;
  ftpClient.setTimeout(timeOut);
  ftpClient.connect(host, userName, password);
  if (!ftpClient.connected) {
    Logger.Status('Ftp server is not connected');
    return new Error(Status.ERROR, 'CONNECTED_FTP_FAILED');
  }
  var allFilesUploader = true;

  parameters.remoteFolder.split('/').forEach(function(folder) {
    if (!ftpClient.cd(folder)) {
      ftpClient.mkdir(folder);
      ftpClient.cd(folder)
    }
  })

  filesList.forEach(function(file) {
    var uploaded = ftpClient.putBinary(file.getName(),file);
    if (!uploaded) {
      allFilesUploader = false;
    } else {
      switch (parameters.ActionForSourceFiles) {
        case 'Archive':
            let fileCompressed = new File(archiveDirectory, file.getName() + '.zip');
            file.zip(fileCompressed);
        case 'Remove':
            file.remove();
        case 'Keep':
        default:
            break;
    }
    }
  })

  if (!allFilesUploader) {
    Logger.error('Not all files were uploaded.');
    return new Error(Status.ERROR, 'ALL_FILES_NOT_UPLOADED')
  }

  return new Status(Status.OK);
}

exports.Run = run;
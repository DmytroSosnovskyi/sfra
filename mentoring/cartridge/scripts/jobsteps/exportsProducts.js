'use strict';

const File = require('dw/io/File');
const CSVStreamWriter = require('dw/io/CSVStreamWriter')
const FileWriter = require('dw/io/FileWriter');
const ProductMgr = require('dw/catalog/ProductMgr');
const Site = require('dw/system/Site').getCurrent();
const StringUtils = require('dw/util/StringUtils');
const Calendar = require('dw/util/Calendar');

var products;
var fileWriter;
var csvStreamWriter;
var targetFile;

exports.beforeStep = function (parameters, stepExecution) {
  products = ProductMgr.queryAllSiteProducts();
  var format = Site.getCustomPreferenceValue('date_format') || 'yyy-MM-dd';
  var date = StringUtils.formatCalendar(new Calendar(), format);

  var targetFolder = new File(parameters.targetFolderPath);
  var fileName = parameters.fileName.replace('{SiteID}', Site.getCurrent().getID()).replace('{DateTime}', date);
  if(!targetFolder.exists()) {
    targetFolder.mkdir();
  }
  targetFile  = new File(targetFolder.getFullPath() + File.SEPARATOR + fileName + '.csv');
  fileWriter = new FileWriter(targetFile, 'UTF-8');
  csvStreamWriter = new CSVStreamWriter(fileWriter, ',');
  csvStreamWriter.writeNext(['ID', 'Type', 'Name', 'Description', 'Status', 'Categories', 'salePrice','regularPrice', 'Availability', 'ATS', "Image"]);
}
exports.getTotalCount = function (parameters, stepExecution) {
  return products.getCount();
}

exports.read = function (parameters, stepExecution) {
  if (products.hasNext()) {
    return products.next();
  }
}

exports.process = function (product, parameters, stepExecution) {
  var product = product;
  var listPriceBook = Site.getCustomPreferenceValue('listPriceDefault') || 'usd-m-list-prices';
  var productType = '';
  var productCategoriesNames = product.getCategories().toArray().map(function(category) {
      return category.ID
  }).join('|');
  if (product.isBundle()) {
    productType = 'productBundle'
  } else if (product.isMaster()) {
    productType = 'master';
  } else if (product.isProductSet()) {
    productType = 'productSet'
  } else if (product.isOptionProduct()) {
    productType = 'optionProduct'
  } else if (product.isVariant()) {
    productType = 'variationProduct';
  }
  return [
    product.ID,
    productType,
    product.name,
    product.shortDescription ? product.shortDescription.getMarkup() : '',
    product.isOnline() ? 'online' : 'ofline',
    productCategoriesNames || '',
    product.getPriceModel().getPrice().getValue(),
    product.getPriceModel().getPriceBookPrice(listPriceBook),
    product.getAvailabilityModel().getAvailabilityStatus(),
    product.getAvailabilityModel().getInventoryRecord() ?
    product.getAvailabilityModel().getInventoryRecord().getATS().valueOf().toString() : 0,
    product.getImage('small') ? product.getImage('small').httpsURL.toString() : ''
  ]
}

exports.write = function (lines, parameters, stepExectuion) {
  lines.toArray().forEach(function (line) {
    csvStreamWriter.writeNext(line.toArray())
  })
}

exports.afterStep = function (success, parameters, stepExecution) {
  csvStreamWriter.close();
  fileWriter.close();
  products.close();
}